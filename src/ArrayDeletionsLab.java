import java.util.Scanner;

public class ArrayDeletionsLab {

    public static void main(String[] args) {
        int arrTest[] = { 1, 2, 3, 4, 5 };
        Scanner kb = new Scanner(System.in);
        System.out.print("Origin Array : ");
        for (int i = 0; i < arrTest.length; i++) {
            System.out.print(arrTest[i] + " ");
        }
        System.out.println();

        System.out.print("Input Index You Want to Delete!! : ");
        int indexDelete = kb.nextInt();
        int newArr[] = deleteElementByIndex(arrTest, indexDelete);
        System.out.print("New Array : ");
        for (int i = 0; i < newArr.length; i++) {
            System.out.print(newArr[i] + " ");
        }
        System.out.println();

        System.out.print("Input Value You Want to Delete!! : ");
        int valueDelete = kb.nextInt();
        int[] newArr2 = deleteElementByValue(arrTest, valueDelete);
        System.out.print("New Array 2 : ");
        for (int i = 0; i < newArr2.length; i++) {
            System.out.print(newArr2[i] + " ");
        }
        System.out.println();
    }

    private static int[] deleteElementByIndex(int[] arr, int index) {
        int newArr[] = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] != arr[index]) {
                newArr[j++] = arr[i];
            }
        }
        return newArr;

    }

    private static int[] deleteElementByValue(int[] arr, int value) {
        int newArr2[] = new int[arr.length - 1];

        for (int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                continue;
            }
            newArr2[j] = arr[i];
            j++;
        }

        return newArr2;
    }

}
