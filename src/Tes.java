public class Tes {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5 };
        int value = 4;
        int[] newArr2 = new int[arr.length - 1];
        int j = 0; // Index for newArr2

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                System.out.println("Value " + value + " found and skipped.");
                continue;
            }
            newArr2[j] = arr[i];
            j++;
        }

        System.out.print("New Array after removing value " + value + ": ");
        for (int i = 0; i < newArr2.length; i++) {
            System.out.print(newArr2[i] + " ");
        }
    }
}
