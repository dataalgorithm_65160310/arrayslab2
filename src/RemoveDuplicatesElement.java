public class RemoveDuplicatesElement {
    public static void main(String[] args) {
        int nums1[] = { 1, 1, 2 };
        int nums2[] = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4 };

        System.out.println("Case 1");
        RemoverDuplicate(nums1);
        System.out.println("Case 2");
        RemoverDuplicate(nums2);
    }

    private static void RemoverDuplicate(int[] nums) {
        System.out.print("[");
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
            if ((i + 1) != nums.length) {
                System.out.print(",");
            }
        }
        System.out.println("]");

        String[] s_nums = new String[nums.length];
        for (int i = 0; i < nums.length; i++) {

            s_nums[i] = "_";
        }
        int k = 0;
        for (int i = 0, j = 0; i < s_nums.length; i++) {

            if (i != 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            s_nums[j] = String.valueOf(nums[i]);
            j++;
            k++;
        }

        System.out.println("k = " + k);
        System.out.print("[");
        for (int i = 0; i < s_nums.length; i++) {
            System.out.print(s_nums[i]);
            if ((i + 1) != s_nums.length) {
                System.out.print(",");
            }
        }
        System.out.println("]");
    }

}
